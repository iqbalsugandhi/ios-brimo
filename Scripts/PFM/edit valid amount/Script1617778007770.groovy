import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

jumlah = Mobile.getAttribute(findTestObject('PFM/edit pengeluaran/XCUIElementTypeTextField - jumlah enable'), 'value', 0)

Mobile.verifyElementExist(findTestObject('PFM/edit pengeluaran/XCUIElementTypeTextField - jumlah enable', [('jumlah') : "$jumlah"]), 
    10)

Mobile.comment(jumlah)

Mobile.clearText(findTestObject('PFM/edit pengeluaran/XCUIElementTypeTextField - jumlah enable'), 0)

Mobile.delay(1)

jumlah2 = Mobile.getAttribute(findTestObject('PFM/edit pengeluaran/XCUIElementTypeTextField - jumlah enable'), 'value', 
    0)

Mobile.comment(jumlah2)

Mobile.tap(findTestObject('PFM/edit pengeluaran/XCUIElementTypeTextField - kategori page edit'), 0)

Mobile.delay(1)

Mobile.tap(findTestObject('PFM/XCUIElementTypeButton - back kategori'), 0)

Mobile.tap(findTestObject('PFM/tambah pengeluaran/XCUIElementTypeImage - BrimoIconAmount'), 0)

Mobile.sendKeys(findTestObject('PFM/edit pengeluaran/XCUIElementTypeTextField - Rp', [(1) : 0]), amount)

Mobile.tap(findTestObject('PFM/tambah pengeluaran/XCUIElementTypeImage - BrimoIconAmount'), 0)

CustomKeywords.'screenshot.capture.Screenshot'()

