import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('General/Database Connect'), [:], FailureHandling.STOP_ON_FAILURE)

if (status.toString() == 'valid') {
	for (int i=1; i<=6; i++) {
//		Mobile.comment('====== ' + PIN.substring(i-1,i) +' ======')
		Mobile.tap(findTestObject('Object Repository/Pin/XCUIElementTypeButton - ' + PIN.substring(i-1,i)), 0)
	}
	
	CustomKeywords.'screenshot.capture.Screenshot'()
	
} else if (status.toString() == 'invalid') {
	for (int i=1; i<=6; i++) {
//		Mobile.comment('====== ' + PIN.substring(i-1,i) +' ======')
		Mobile.tap(findTestObject('Object Repository/Pin/XCUIElementTypeButton - ' + PIN.substring(i-1,i)), 0)
	}
	Mobile.delay(1)
	
	CustomKeywords.'screenshot.capture.Screenshot'()
	
} else if (status.toString() == "valid after try") {
	String[] pinArray = ["111111"]
		for (int j=1; j<=6; j++) {
//			Mobile.comment('==== ' + pinArray[0].substring(j-1,j) + ' ====')
			Mobile.tap(findTestObject('Object Repository/Pin/XCUIElementTypeButton - '+ pinArray[0].substring(j-1,j)), 0)
		}
		
		Mobile.delay(1)
		
		CustomKeywords.'screenshot.capture.Screenshot'()
		
		Mobile.delay(3)
		
		Mobile.tap(findTestObject('Object Repository/Wallet Nominal Form/XCUIElementTypeButton - Top Up'), 0)
		
		for (int i=1; i<=6; i++) {
//			Mobile.comment('====== ' + PIN.substring(i-1,i) +' ======')
			Mobile.tap(findTestObject('Object Repository/Pin/XCUIElementTypeButton - ' + PIN.substring(i-1,i)), 0)
		}
		
		Mobile.delay(3)
		
		CustomKeywords.'screenshot.capture.Screenshot'()
		
} else if (status.toString() == "invalid 3x") {
	String[] pinArray = ["111111", "111999", "999999"]
	for (int i=0; i<3; i++) {
		for (int j=1; j<=6; j++) {
//			Mobile.comment('====' + pinArray[i].substring(j-1,j) + '====')
			Mobile.tap(findTestObject('Object Repository/Pin/XCUIElementTypeButton - '+ pinArray[i].substring(j-1,j)), 0)
		}
		Mobile.delay(1)
		CustomKeywords.'screenshot.capture.Screenshot'()
		
		Mobile.delay(3)
		
		if(i!=2) {
			Mobile.tap(findTestObject('Object Repository/Wallet Nominal Form/XCUIElementTypeButton - Top Up'), 0)
		}
	}
	
	Mobile.delay(1)
	CustomKeywords.'screenshot.capture.Screenshot'()
	
}

Mobile.delay(3)

CustomKeywords.'database.methods.executeUpdate'(("UPDATE ibank.tbl_user SET status=1,login_retry=0 WHERE username='"+ GlobalVariable.username +"'"))

WebUI.callTestCase(findTestCase('General/Database Close'), [:], FailureHandling.STOP_ON_FAILURE)